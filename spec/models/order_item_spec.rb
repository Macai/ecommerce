require 'spec_helper'

describe OrderItem do
	let(:order) { Order.create }
	let(:product) { Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22 ) }
	let(:order_item_with_no_order) { OrderItem.create(:product_id => product.id, :quantity => 3) }
	let(:order_item_with_no_product) { OrderItem.create(:order_id => order.id, :quantity => 3) }
	let(:order_item_with_no_quantity) { OrderItem.create(:order_id => order.id, :product_id => product.id) }
	let(:full_order_item) { OrderItem.create(:order_id => order.id, :product_id => product.id, :quantity => 3) }
	
	it "should only be valid if it has an order" do
		expect(order_item_with_no_order.valid?).to be_false
		expect(order_item_with_no_order.order).to be_false
		
		expect(full_order_item.order).to be_true
		expect(full_order_item.valid?).to be_true
	end
	
	it "should only be valid if it has a product" do
		expect(order_item_with_no_product.valid?).to be_false
		expect(order_item_with_no_product.product).to be_false
		
		expect(full_order_item.product).to be_true
		expect(full_order_item.valid?).to be_true
	end

	it "should only be valid if it has a quantity that is at least one" do
		expect(order_item_with_no_quantity.valid?).to be_false
		expect(order_item_with_no_quantity.quantity).to be_false
		
		expect(full_order_item.quantity).to be_true
		expect(full_order_item.valid?).to be_true
	end
end
