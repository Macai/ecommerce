require 'spec_helper'
require 'digest/sha1'
describe User do
  let(:user) { User.create(:name => "Joe Higgins", :email => "joe@higgins.name", :password_hash => "blah blah" ) }
  let(:nameless_user) { User.create(:email => "joe@higgins.name", :password_hash => "blahblah") }
  let(:emailless_user) { User.create(:name => "Joe Higgins", :password_hash => "blahblah") }
  let(:password_hashless_user) { User.create(:name => "Joe Higgins", :email => "joe@higgins.name") }

 #~ context "that is a customer (which is all of them)" do
	#~ it "should be allowed to add products to cart" do
		#~ expect(user.can_add_products_to_cart?).to eq(true)
	#~ end
	#~ 
	#~ it "should be allowed to search for products" do
		#~ expect(user.can_search_products_in_catalog?).to eq(true)
	#~ end
	#~ 
	#~ it "should be able to add a virtual cart with products" do
		#~ first_product = Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22)
		#~ second_product = Product.create(:name => "Some other product", :cost => 433.33, :description => "This does something else", :category_id => 1, :price => 32)
		#~ 
		#~ user.add_to_cart(:product => first_product, :quantity => 2)
		#~ user.add_to_cart(:product => first_product, :quantity => 3)
		#~ user.add_to_cart(:product => second_product, :quantity => 4)
		#~ user.add_to_cart(:product => second_product, :quantity => -4)
#~ 
		#~ expect(user.current_cart_contents).to eq({ first_product.id => 5 })
	#~ end
#~ 
	#~ it "should be able to empty that cart" do
		#~ first_product = Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22)
		#~ user.add_to_cart(:product => first_product, :quantity => 2)
		#~ user.empty_cart
		#~ expect(user.current_cart_contents).to have(0).items
	#~ end
	#~ 
	#~ it "should be allowed to checkout" do
		#~ expect(user.can_checkout?).to be_true
	#~ end
	#~ 
	#~ it "should be allowed to view order history" do
		#~ expect(user.can_view_purchase_history?).to be_true
	#~ end
 #~ end
 
  it "should have a name" do
    expect(user.valid?).to be_true
    expect(nameless_user.valid?).to be_false
  end
  it "should have a email" do
    expect(user.valid?).to be_true
    expect(emailless_user.valid?).to be_false
  end
  it "should have a password_hash" do
    expect(user.valid?).to be_true
    expect(password_hashless_user.valid?).to be_false
  end

 
 context "that is a storekeeper" do
	it "should be allowed to add products to the catalog" do
		user.is_storekeeper = true
		expect(user.can_add_products_to_catalog?).to be_true
		
		user.is_storekeeper = false
		expect(user.can_add_products_to_catalog?).to be_false
	end
	
	it "should be allowed to update product information" do
		user.is_storekeeper = true
		expect(user.can_update_catalog_entries?).to be_true
		
		user.is_storekeeper = false
		expect(user.can_update_catalog_entries?).to be_false
	end
	
	it "should be allowed to remove products" do
		user.is_storekeeper = true
		expect(user.can_remove_entries_from_catalog?).to be_true
		
		user.is_storekeeper = false
		expect(user.can_remove_entries_from_catalog?).to be_false
	end
	
	it "should be allowed to view a list of completed orders" do
		user.is_storekeeper = true
		expect(user.can_view_completed_orders?).to be_true
		
		user.is_storekeeper = false
		expect(user.can_view_completed_orders?).to be_false
	end
 end
end
