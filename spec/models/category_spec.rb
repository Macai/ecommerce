require 'spec_helper'

describe Category do
  let(:category) { Category.create( :name => "Vegetables", :description => "Tomatoes don't count") }
  let(:nameless_category) { Category.create( :description => "Tomatoes don't count") }
  let(:descriptionless_category) { Category.create( :name => "Vegetables") }
  
  it "should have a name" do
    expect(category.valid?).to be_true
    expect(nameless_category.valid?).to be_false
  end

  it "should have a description" do
    expect(category.valid?).to be_true
    expect(descriptionless_category.valid?).to be_false
  end
end
