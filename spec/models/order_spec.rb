require 'spec_helper'

describe Order do
	let(:order) { Order.create }
	let(:product) { Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22 ) }
	it "should have many order items" do
		expect(order.order_items).to be_true
	end
	
	it "should be able to add items" do
		order.add_item product, 3
		
		expect(order.order_items).to have_exactly(1).items
	end
	
	it "should be able to get rid of items" do
		order.add_item product, 3
		order.remove_item product
		expect(order.order_items).to have_exactly(0).items
	end
  
  it "should be able to become checked out" do
    order.is_now_checked_out
    order.reload
    expect(order.completed_at).to_not be_nil
  end
end
