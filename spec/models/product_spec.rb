require 'spec_helper'

describe Product do
	let(:product) { Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22 ) }
  let(:nameless_product) { Product.create(:cost => 23.33, :description => "This thing does stuff", :category_id => 0, :price => 32.22 ) }
  let(:descriptionless_product) { Product.create(:name => "Some product", :cost => 23.33, :category_id => 0, :price => 32.22 ) }
  let(:categoryless_product) { Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :price => 32.22 ) }
  let(:priceless_product) { Product.create(:name => "Some product", :cost => 23.33, :description => "This thing does stuff", :category_id => 0 ) }

  it "has a name" do
    expect(product.valid?).to be_true
    expect(nameless_product.valid?).to be_false
  end

  it "has a description" do
    expect(product.valid?).to be_true
    expect(descriptionless_product.valid?).to be_false
  end

  it "has a category" do
    expect(product.valid?).to be_true
    expect(categoryless_product.valid?).to be_false
  end

  it "has a price" do
    expect(product.valid?).to be_true
    expect(priceless_product.valid?).to be_false
  end


end
