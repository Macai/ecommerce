require 'spec_helper'

feature "View history" do
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  let(:shopkeeper) { User.where(:email => "dont_need_no_tld@yhwh").first }
  
  scenario "is a shopkeep" do
    ApplicationHelper.current_user = shopkeeper
    
    visit "order/history"
    
    expect(page).to have_content("Viewing ALL Orders")
  end
  
  scenario "is just a customer" do
    ApplicationHelper.current_user = customer
    
    visit "order/history"
    
    expect(page).to have_content("Viewing YOUR Orders")
  end
end
