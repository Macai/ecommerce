require 'spec_helper'
require 'application_helper'
feature "List all products" do
  scenario "user looks at products" do
    visit product_index_path
    
    expect(page).to have_content("Products List!")
    expect(page).to have_content(Category.all.first.name)
    expect(page).to have_content(Category.all.first.products.first.name)
    expect(page).to have_content("Your cart includes")
  end
end

feature "Add a product to a cart" do
  scenario "whenever" do
    product = Product.all.first
    visit "/product/#{product.id}/add_to_cart"
    
    expect(page).to have_content("Added #{product.name} to your shopping cart!")
  end
end

feature "Remove a product from a cart" do
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  scenario "whenever" do
    product = Product.all.first
    customer = User.where(:email => "no_email@nowhere.com").first
    customer.current_order.add_item(product, 1)
    
    visit "/product/#{product.id}/remove_from_cart"
    
    expect(page).to have_content("You have removed #{product.name} from cart!")
  end
  
end


feature "Create new product entirely" do
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  let(:shopkeeper) { User.where(:email => "dont_need_no_tld@yhwh").first }
  
  scenario "when not a shopkeeper" do
    ApplicationHelper.current_user = customer
    
    visit new_product_path
    
    expect(page).to have_content("You can't make new products!")
  end
  
  scenario "when a shopkeeper" do
    ApplicationHelper.current_user = shopkeeper
    
    visit new_product_path
    
    expect(page).to have_content("Product name: ")
  end
end


feature "View a product for update" do
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  let(:shopkeeper) { User.where(:email => "dont_need_no_tld@yhwh").first }
  
  scenario "when not a shopkeeper" do
    ApplicationHelper.current_user = customer
    visit "#{product_show_path}/?id=#{Product.first.id}"
    
    expect(page).to have_content("Only shopkeepers can do that.")
  end
  
  scenario "when a shopkeeper" do
    ApplicationHelper.current_user = shopkeeper
    visit "#{product_show_path}/?id=#{Product.first.id}"
    
    expect(page).to have_content("Update Old Product!")
  end
end

feature "Actually update the product" do 
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  let(:shopkeeper) { User.where(:email => "dont_need_no_tld@yhwh").first }
  
  scenario "when not a shopkeeper" do
    ApplicationHelper.current_user = customer
    visit product_edit_path
    
    expect(page).to have_content("Only shopkeepers can do that.")
  end
  
  scenario "when a shopkeeper" do
    ApplicationHelper.current_user = shopkeeper
    
    visit "product/2/edit"
    fill_in 'Product name', :with => "Some stuff"
    fill_in 'Product price', :with => "2.21"
    fill_in 'Product cost', :with => "1.22"
    fill_in 'Product category', :with => "2"
    fill_in 'Product description', :with => "Somedssd stuff"
    
    click_button "Update Product"
    
    expect(page).to have_content("Product price:")
  end  
end

feature "Delete products from the catalog" do
  let(:customer) { User.where(:email => "no_email@nowhere.com").first }
  let(:shopkeeper) { User.where(:email => "dont_need_no_tld@yhwh").first }
  let(:product) { Product.find(3) }
  scenario "when not a shopkeeper" do
    ApplicationHelper.current_user = customer
    visit "product/2/destroy"
    
    expect(page).to have_content("Only shopkeepers can do that.")
  end
  scenario "when a shopkeeper" do
    ApplicationHelper.current_user = shopkeeper
    
    visit "product/2/destroy"
    
    expect(page).to have_content("Product deleted!")
  end
  scenario "when a shopkeeper, but there is a completed order" do
    ApplicationHelper.current_user = shopkeeper
    shopkeeper.current_order.add_item product, 1
    shopkeeper.current_order.is_now_checked_out
    shopkeeper.current_order.save
    visit "product/3/destroy"
    
    expect(page).to have_content("That has a completed order! No delete!")
  end
end
