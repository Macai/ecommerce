require 'spec_helper'

feature "Checkout" do

  scenario "should always let it check out, no PayPal implemention yet" do
    current_user = User.where(:email => "no_email@nowhere.com").first
    order = current_user.current_order
    order.shipment_address = "Joe Higgins\n123 Test Drive\nSomewhere, NY, 12345"
    order.save
    visit "/order/#{order.id}/verify"
    
    expect(page).to have_content("Your order will be shipped")
  end
end
