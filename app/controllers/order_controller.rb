class OrderController < ApplicationController
  def verify
    order = Order.where(:id => params[:order_id]).first
    
    #TODO: Make it actually verify it here
    
    order.is_now_checked_out
    redirect_to root_url, notice: "You've been checked out! Your order will be shipped!"
  end
  
  def history
    if current_user.is_storekeeper
      @scope = "ALL"
      @orders = Order.order("completed_at DESC").load
    else
      @scope = "YOUR"
      @orders = Order.where(:user_id => current_user.id).order("completed_at DESC")
    end
  end
end
