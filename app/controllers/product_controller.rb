class ProductController < ApplicationController
  def index
    redirect_to root_url
  end
  def edit
    if !current_user || !current_user.can_update_catalog_entries?
      redirect_to root_url, notice: "Only shopkeepers can do that."
      return
    end
        
    @product = Product.where(:id => params[:id]).first
    redirect_to root_url, notice: "No such product" unless @product
  end
  
  def show
    if !current_user || !current_user.can_update_catalog_entries?
      redirect_to root_url, notice: "Only shopkeepers can do that."
      return
    end
        
    @product = Product.where(:id => params[:id]).first
    redirect_to root_url, notice: "No such product" unless @product
  end

  def update
    if !current_user || !current_user.can_update_catalog_entries?
      redirect_to root_url, notice: "Only shopkeepers can do that."
      return
    end
    
    @product = Product.where(:id => params[:id]).first
    
    redirect_to root_url, notice: "No such product" unless @product
    
    @product.update_attributes(product_params)
  end

  def create
    if !current_user || !current_user.can_update_catalog_entries?
      redirect_to root_url, notice: "Only shopkeepers can do that."
      return
    end
    
    product = Product.create(:name => params[:product][:name], :description => params[:product][:description], :cost => params[:product][:cost], :category_id => params[:product][:category_id], :price => params[:product][:price])
    
    redirect_to product_new_path unless product.valid?
  end

  def destroy
    redirect_to root_url, notice: "Only shopkeepers can do that." unless current_user && current_user.can_update_catalog_entries?
  end
  
  def remove_from_cart
	product = Product.where(:id => params[:product_id]).first
    if !product
      redirect_to root_url, notice: "That product doesn't exist!"
      return
    end
    
    current_user.current_order.remove_item product
    redirect_to root_url, notice: "You have removed #{product.name} from cart!"
  end
  
  def add_to_cart
    product = Product.where(:id => params[:product_id]).first
    
    if !product
      redirect_to root_url, notice: "That product doesn't exist!"
      return
    end

    quantity = 1
    if params[:quantity].class == String && params[:quantity].match(/^[0-9]+$/)
      quantity = params[:quantity].to_i
    end
    
    current_user.current_order.add_item product, quantity
    
    redirect_to root_url, notice: "Added #{product.name} to your shopping cart!"
  end
  
  def new
	redirect_to root_url, notice: "You can't make new products!" unless current_user.is_storekeeper
  end
  
  def destroy
    if !current_user || !current_user.can_update_catalog_entries?
      redirect_to root_url, notice: "Only shopkeepers can do that."
      return
    end
  
    @product = Product.where(:id => params[:id]).first
    
    OrderItem.where(:product_id => @product.id).each do |oi|
      if oi.order.completed_at
        redirect_to root_url, notice: "That has a completed order! No delete!"
        return
      end
    end
    
    @product.destroy if @product
    
    
    redirect_to root_url, notice: "Product deleted!"
  end
  
  private
  
  def product_params
      params.require(:product).permit(:name, :description, :cost, :price, :category_id)
  end
end
