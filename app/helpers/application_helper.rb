module ApplicationHelper
  def self.current_user=(usr)
    @@injected_user = usr
  end
  
  def current_user
    if defined?(@@injected_user)
      @@injected_user
    else
      @current_user ||= User.where(:id => 2).first
    end
  end
end
