class Order < ActiveRecord::Base
  belongs_to :user
  has_many :order_items
  
  def add_item(product, quantity)
    order_items.each do |oi|
      if oi.product.id == product.id
        oi.quantity += 1
        oi.save
        return
      end
    end
    
    order_item = OrderItem.create(:order_id => self.id, :product_id => product.id, :quantity => quantity)
    
    order_items << order_item
    self.save
  end

  def remove_item(product)
    order_items.each do |oi|
      if product.id == oi.product.id
        oi.destroy!
        self.reload
        return
      end
    end
  end

  def is_now_checked_out
    self.completed_at = DateTime.now
    save
  end
end
