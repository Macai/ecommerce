class User < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :email
  validates_presence_of :password_hash
  
  has_many :orders

	def can_add_products_to_cart?
		true # we might one day want to make it so you can ban certain users or something
	end

	def can_search_products_in_catalog?
		true # we might also want to ban this if they spam the server with too many search requests
	end

	def can_checkout?
		true # in case of bans
	end

	def can_view_purchase_history?
		true # in case of bans
	end

	def can_remove_products_from_catalog?
		is_storekeeper == true
	end

	def can_add_products_to_catalog?
		is_storekeeper == true
	end

	def can_view_completed_orders?
		is_storekeeper == true
	end

	def can_update_catalog_entries?
		is_storekeeper == true
	end

	def can_remove_entries_from_catalog?
		is_storekeeper == true
	end

	def empty_cart
		@cart = {}
	end

  def current_order
    candidates = orders.select { |ord| ord.completed_at == nil }
    candidates << Order.create( :user_id => self.id ) if candidates.size == 0
    self.reload
    candidates[0]
  end
  
	def add_to_cart(args)
		# refuse to continue if an input is not valid
		return if args[:product].class != Product
		return if args[:quantity].class != Fixnum
		
		current_order.add_item args[:product], args[:quantity]
	end
	
	def current_cart_contents
		@cart ||= {}
	end
end
