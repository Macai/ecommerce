# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Ecommerce::Application.config.secret_key_base = 'deafa840bf66e1f6a1bc00147deb2cb8d3dbe6ae4ea7d0d03df1a3ad22aea3ec830f1b12598243cfef002fc12f1aafe62df5c04535e5bdc43284c81c1312aff1'
