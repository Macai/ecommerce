class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.datetime :completed_at
      t.string :shipment_address

      t.timestamps
    end
  end
end
