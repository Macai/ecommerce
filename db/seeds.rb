# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

fruits = Category.create(name: "Fruits", description: "Some fruits.")

["oranges", "apples", "pears", "peaches"].each do |fruit|
  Product.create(name: fruit, description: "#{fruit.capitalize}!", price: 2, category_id: fruits.id, cost: 1)
end

vegetables = Category.create(name: "Vegetables", description: "Tomatoes don't count!")

["lettuce", "carrots", "beets", "onions"].each do |vegetable|
  Product.create(name: vegetable, description: "#{vegetable.capitalize}!", price: 2, category_id: vegetables.id, cost: 1)
end
User.create( :name => "Anonymous Customer", :email => "no_email@nowhere.com", :password_hash => "literally not crackable")
User.create( :name => "YHWH", :email => "dont_need_no_tld@yhwh", :password_hash => "literally not crackable", :is_storekeeper => true)
